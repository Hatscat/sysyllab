# Sysyllab

A tool to read stories for kids, for web and mobile (PWA).

## Technical stack

* UI: [P5](https://p5js.org/)
* Data storage: [localStorage](https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API/Using_the_Web_Storage_API)
* Bundle: [Parcel](https://parceljs.org/getting_started.html)
* PWA: [Workbox](https://developers.google.com/web/tools/workbox/)
* Unit test: *not yet*
* E2E test: *not yet*

## Development prerequisites

* [npm](https://nodejs.org)
* [Yarn](https://yarnpkg.com)

## Installation

To install all dependencies, run the following command from the project directory:

```sh
yarn
```

### Recommended IDE

[Visual Studio Code](https://code.visualstudio.com/)

## Run the app on web browser

```sh
yarn dev
```

Then open your browser at [`localhost:1234`](http://localhost:1234)

## Build the app (bundle with parcel)

```sh
yarn build
```

## Run tests

```sh
yarn validate
```

## Fix dev environment issues

```sh
yarn rescue
```
