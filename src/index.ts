import p5 from 'p5';
import { configureStore } from './configureStore';
import storiesObj from '../resources/stories.json';
import { Action, getPageText, getCurrentSyllabe, ReadingStep } from './reducer';
import { sayInFrench } from './speechSynth';

// TODO: to uncomment
// if ('serviceWorker' in navigator) {
//     window.addEventListener('load', (): void => {
//         navigator.serviceWorker.register('./service-worker.js');
//     });
// }

const { store } = configureStore();
const TEXT_SIZE = 20;

const sketch = (p: p5): void => {
    p.preload = (): void => {
        store.dispatch({ type: Action.LOAD_STORIES, payload: storiesObj.stories });
    };

    p.setup = (): void => {
        console.log('store.getState()', store.getState());
        p.createCanvas(p.windowWidth, p.windowHeight);
        p.textAlign(p.LEFT, p.TOP);
        p.textSize(TEXT_SIZE);
        p.textLeading(TEXT_SIZE * 1.2);
    };

    p.windowResized = (): void => {
        p.resizeCanvas(p.windowWidth, p.windowHeight);
    };

    p.mouseClicked = (): void => {
        const { syllabeBetweenTexts, readingStep } = store.getState();
        switch (readingStep) {
            case ReadingStep.OPENING:
                store.dispatch({ type: Action.NEXT_SYLLABE });
                store.dispatch({ type: Action.NEXT_READING_STEP });
                sayInFrench(getCurrentSyllabe(store.getState()), { rate: 1 });
                break;
            case ReadingStep.READING:
                if (syllabeBetweenTexts === null) {
                    sayInFrench(getPageText(store.getState()));
                    store.dispatch({ type: Action.NEXT_READING_STEP });
                } else {
                    store.dispatch({ type: Action.NEXT_SYLLABE });
                    sayInFrench(getCurrentSyllabe(store.getState()), { rate: 1 });
                }
                break;
            case ReadingStep.SUMMARIZE:
                store.dispatch({ type: Action.NEXT_PAGE });
                store.dispatch({ type: Action.NEXT_READING_STEP });
                break;
        }
    };

    p.draw = (): void => {
        const { syllabeBetweenTexts, readingStep } = store.getState();
        const MARGIN = 32;
        const READ_COLOR = p.color(255, 0, 0);

        p.background(242, 242, 242);

        if (syllabeBetweenTexts) {
            p.noFill();
            p.stroke(0);

            p.text(syllabeBetweenTexts.textBefore, MARGIN, MARGIN, p.width - MARGIN, p.height - MARGIN);
            p.fill(READ_COLOR);
            p.stroke(READ_COLOR);
            p.text(
                syllabeBetweenTexts.syllabe,
                MARGIN + p.textWidth(syllabeBetweenTexts.textBefore),
                MARGIN,
                p.width - MARGIN,
                p.height - MARGIN,
            );

            p.fill(0);
            p.stroke(0);

            p.text(
                syllabeBetweenTexts.textAfter,
                MARGIN + p.textWidth(syllabeBetweenTexts.textBefore + syllabeBetweenTexts.syllabe),
                MARGIN,
                p.width - MARGIN,
                p.height - MARGIN,
            );
        } else {
            if (readingStep === ReadingStep.SUMMARIZE) {
                p.fill(READ_COLOR);
                p.stroke(READ_COLOR);
            } else {
                p.fill(0);
                p.stroke(0);
            }
            p.text(getPageText(store.getState()) || '', MARGIN, MARGIN, p.width - MARGIN, p.height - MARGIN);
        }
    };
};

new p5(sketch);

// https://www.silabas.net/index-fr.php?lang=%2Findex-fr.php&p=courrais&button=S%C3%A9par%C3%A9es+et+c%C3%A9sure+syllabes
