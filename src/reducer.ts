/**
 * Actions
 */

export enum Action {
    NOTHING,
    LOAD_STORIES,
    NEXT_READING_STEP,
    NEXT_PAGE,
    NEXT_SYLLABE,
}

export enum ReadingStep {
    OPENING,
    READING,
    SUMMARIZE,
}

interface NoAction {
    type: typeof Action.NOTHING;
}

interface LoadStoriesAction {
    type: typeof Action.LOAD_STORIES;
    payload: Story[];
}

interface NextReadingStepAction {
    type: typeof Action.NEXT_READING_STEP;
}

interface NextPageAction {
    type: typeof Action.NEXT_PAGE;
}

interface NextSyllabeAction {
    type: typeof Action.NEXT_SYLLABE;
}

export type ActionType = NoAction | LoadStoriesAction | NextReadingStepAction | NextPageAction | NextSyllabeAction;

/**
 * State
 */

export interface Story {
    id: string;
    title: string;
    iconUrl: string | null;
    pages: readonly string[];
}

export interface SyllabeBetweenTexts {
    textBefore: string;
    syllabe: string;
    textAfter: string;
}

export interface State {
    readingStep: ReadingStep;
    storyById: { [id: string]: Story };
    storyId: Story['id'] | null;
    pageIndex: number;
    syllabeIndex: number;
    syllabeBetweenTexts: SyllabeBetweenTexts | null;
}

export const initialState: State = {
    readingStep: ReadingStep.OPENING,
    storyById: {},
    storyId: null,
    pageIndex: 0,
    syllabeIndex: 0,
    syllabeBetweenTexts: null,
};

/**
 * local Helpers
 */

const cleanText = (text: string): string => {
    return text.replace(/_/g, '');
};

const extractSyllabe = (text: string, syllabeIndex: number): RegExpExecArray | null => {
    const syllabeRegex = /[a-zA-ZÀ-ÿ'’]+/g;
    for (let _i = 0; _i < syllabeIndex; _i++) {
        syllabeRegex.exec(text);
    }
    return syllabeRegex.exec(text);
};

const extractSyllabeBetweenTexts = (page: string, syllabeIndex: number): SyllabeBetweenTexts | null => {
    const maybeSyllabe = extractSyllabe(page, syllabeIndex);
    if (!maybeSyllabe) return null;
    return {
        textBefore: cleanText(page.slice(0, maybeSyllabe.index)),
        syllabe: maybeSyllabe[0],
        textAfter: cleanText(page.slice(maybeSyllabe.index + maybeSyllabe[0].length)),
    };
};

/**
 * Selectors
 */

export const getCurrentStory = (state: State): Story | null => {
    const { storyById, storyId } = state;
    return storyId != null ? storyById[storyId] : null;
};

export const getCurrentPage = (state: State): string | null => {
    const { pageIndex } = state;
    const maybeStory = getCurrentStory(state);
    return maybeStory != null ? maybeStory.pages[pageIndex] : null;
};

export const getPageText = (state: State): string | null => {
    const page = getCurrentPage(state);
    return page ? cleanText(page) : null;
};

export const getCurrentSyllabe = (state: State): string | null => {
    const { syllabeBetweenTexts } = state;
    return syllabeBetweenTexts != null ? syllabeBetweenTexts.syllabe : null;
};

/**
 * Reducer
 */

export function reducer(state: State = initialState, action: ActionType): State {
    switch (action.type) {
        case Action.LOAD_STORIES: {
            const storyById = action.payload.reduce(
                (result, story): State['storyById'] => ({ ...result, [story.id]: story }),
                {},
            );
            const firstStoryId = action.payload[0].id;
            return { ...state, storyById, storyId: firstStoryId };
        }
        case Action.NEXT_READING_STEP: {
            const nexSteps = {
                [ReadingStep.OPENING]: ReadingStep.READING,
                [ReadingStep.READING]: ReadingStep.SUMMARIZE,
                [ReadingStep.SUMMARIZE]: ReadingStep.OPENING,
            };
            return { ...state, readingStep: nexSteps[state.readingStep] };
        }
        case Action.NEXT_PAGE: {
            const story = getCurrentStory(state);
            const nextIndex = state.pageIndex + 1;
            const pageIndex = story == null || story.pages.length === nextIndex ? 0 : nextIndex;
            return { ...state, pageIndex, syllabeIndex: -1 };
        }
        case Action.NEXT_SYLLABE: {
            const page = getCurrentPage(state);
            const nextIndex = state.syllabeIndex + 1;
            const syllabeBetweenTexts = page ? extractSyllabeBetweenTexts(page, nextIndex) : null;
            return { ...state, syllabeIndex: nextIndex, syllabeBetweenTexts };
        }
        default:
            return state;
    }
}
