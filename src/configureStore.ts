import { createStore, Store } from 'redux';
import { reducer, initialState } from './reducer';
import { persistStore, persistReducer, Persistor } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const persistConfig = {
    key: 'root',
    storage,
};

const persistedReducer = persistReducer(persistConfig, reducer);

export function configureStore(onComplete?: () => void): { store: Store; persistor: Persistor } {
    const store = createStore(persistedReducer, initialState);
    const persistor = persistStore(store, undefined, onComplete);
    return {
        store,
        persistor,
    };
}
