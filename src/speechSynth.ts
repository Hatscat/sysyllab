// doc: https://flaviocopes.com/speech-synthesis-api/

const synth = window.speechSynthesis;

interface SpeechOptions {
    pitch?: number; // 0 to 2
    volume?: number; // 0 to 1
    rate?: number; // 0.1 to 10
}

const defaultOptions = Object.freeze({
    pitch: 1,
    volume: 1,
    rate: 1,
});

const speak = (text: string, lang: string, options: SpeechOptions = defaultOptions): void => {
    const { pitch, volume, rate } = options;
    const utterance = new SpeechSynthesisUtterance(text);
    utterance.lang = lang;
    utterance.pitch = pitch != null ? pitch : defaultOptions.pitch;
    utterance.volume = volume != null ? volume : defaultOptions.volume;
    utterance.rate = rate != null ? rate : defaultOptions.rate;
    synth.speak(utterance);
};

export const sayInFrench = (text: string | null, options?: SpeechOptions): void => {
    if (text != null) {
        speak(text, 'fr-FR', options);
    }
};
