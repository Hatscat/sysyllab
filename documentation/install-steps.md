# dev env install steps

## project dependencies

``` sh
yarn add p5
yarn add redux redux-persist
```

## project dev dependencies

``` sh
yarn add -D typescript
yarn add -D eslint @typescript-eslint/parser @typescript-eslint/eslint-plugin
yarn add -D prettier eslint-config-prettier eslint-plugin-prettier
yarn add -D parcel-bundler
yarn add -D husky
yarn add -D @types/p5
```

## typescript config setup

``` sh
yarn tsc --init
```
